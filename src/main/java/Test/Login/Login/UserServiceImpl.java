package Test.Login.Login;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public  class UserServiceImpl implements UserService{


private UserRepository userRepository;

@Autowired
private BCryptPasswordEncoder passwordEncoder;

public UserServiceImpl(UserRepository userRepository){

        super();
        this.userRepository = userRepository;


}


    @Override
    public Users save(UserRegistrationDto registrationDto){
Users users = new Users(registrationDto.getFirstName(), registrationDto.getLastName(), registrationDto.getEmail(), passwordEncoder.encode(registrationDto.getPassword()), 
Arrays.asList(new Role("ROLE_USER")));

return userRepository.save(users);

    }

    @Override
    public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException{

        Users users = userRepository.findByEmail(username);
        if (users == null){

            throw new UsernameNotFoundException("Invalid username or password");
        }
        return new org.springframework.security.core.userdetails.User(users.getEmail(), users.getPassword(), mapRolesToAuthorities(users.getRoles()));
    }
private Collection <? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){

    return  roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());


    
    
}
    
   

    
}