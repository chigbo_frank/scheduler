package Test.Login.Login;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    
    //Users save(UserRegistrationDto registrationDto);

    Users save(UserRegistrationDto registrationDto);

}